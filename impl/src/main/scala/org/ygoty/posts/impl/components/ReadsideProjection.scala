package org.ygoty.posts.impl.components

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ShardedDaemonProcess
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.Offset
import akka.projection.{ProjectionBehavior, ProjectionId}
import akka.projection.eventsourced.EventEnvelope
import akka.projection.eventsourced.scaladsl.EventSourcedProvider
import akka.projection.scaladsl.{ExactlyOnceProjection, SourceProvider}
import akka.projection.slick.{SlickHandler, SlickProjection}
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

object ReadsideProjection {

  private def sourceProvider[Event](tag: String, system: ActorSystem[_]): SourceProvider[Offset, EventEnvelope[Event]] =
    EventSourcedProvider
      .eventsByTag[Event](
        system = system,
        readJournalPluginId = CassandraReadJournal.Identifier,
        tag = tag
      )

  private def projection[Event](
    projectionName: String,
    dbConfig: DatabaseConfig[PostgresProfile],
    handler: SlickHandler[EventEnvelope[Event]],
    tag: String
  )(implicit actorSystem: ActorSystem[_]): ExactlyOnceProjection[Offset, EventEnvelope[Event]] =
    SlickProjection
      .exactlyOnce[Offset, EventEnvelope[Event], PostgresProfile](
        projectionId = ProjectionId(projectionName, tag),
        sourceProvider(tag, actorSystem),
        dbConfig,
        handler = () => handler
      )

  def apply[Event](
    projectionName: String,
    dbConfig: DatabaseConfig[PostgresProfile],
    handler: SlickHandler[EventEnvelope[Event]],
    tags: Vector[String]
  )(implicit actorSystem: ActorSystem[_]) = {
    //Dangerous!!!! Remove for production or all will burn.
//    SlickProjection
//      .dropTablesIfExists(dbConfig)
    // Setup the offset table
//    SlickProjection
//      .createTablesIfNotExists(dbConfig)

    // Setup the readside projection
    ShardedDaemonProcess(actorSystem)
      .init[ProjectionBehavior.Command](
        name = projectionName.toLowerCase(),
        numberOfInstances = tags.size,
        behaviorFactory = (i: Int) => ProjectionBehavior(projection(projectionName, dbConfig, handler, tags(i))),
        stopMessage = ProjectionBehavior.Stop
      )
  }

}
