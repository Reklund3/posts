package org.ygoty.posts.impl.components

import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import org.ygoty.posts.impl.entities.Post

private[impl] trait ClusterShardingExtensions {
  def clusterSharding: ClusterSharding

  def postRef(id: String) = clusterSharding.entityRefFor(Post.TypeKey, id)

}
