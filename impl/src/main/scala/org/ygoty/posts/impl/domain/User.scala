package org.ygoty.posts.impl.domain

import org.ygoty.posts.impl.entities.Post.CborSerializer

import java.util.UUID

case class User(id: UUID, username: String, email: String) extends CborSerializer