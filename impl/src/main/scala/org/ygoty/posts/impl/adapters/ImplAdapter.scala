package org.ygoty.posts.impl.adapters

import org.ygoty.posts.impl.grpc
import org.ygoty.posts.impl.entities.Post

object ImplAdapter {
  implicit def postToGRPC(post: Post.Posted): grpc.Post = {
    grpc.Post(post.id.toString, post.userId.toString, post.postBody.value)
  }
}
