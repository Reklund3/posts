package org.ygoty.posts.impl.entities

import akka.Done
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{EntityContext, EntityTypeKey}
import akka.pattern.StatusReply
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import eu.timepit.refined.types.all.NonEmptyString

import java.time.{Clock, Instant}
import java.util.UUID
import scala.concurrent.duration.DurationInt

object Post {
  final val TypeKey = EntityTypeKey[Command]("Post")

  trait CborSerializer

  //**********************************************************************************************************************
  //  Post Commands
  //**********************************************************************************************************************

  sealed trait Command extends CborSerializer

  case class Create(
                     postId: UUID,
                     userId: UUID,
                     postBody: NonEmptyString,
                     replyTo: ActorRef[StatusReply[Done]]
                   ) extends Command

  case class Update(
                     body: NonEmptyString,
                     replyTo: ActorRef[StatusReply[Done]]
                   ) extends Command

  /** Command to delete a post
   *
   * @param userId     Id of the user requesting the delete
   * @param replyTo
   */
  case class Delete(userId: UUID, replyTo: ActorRef[StatusReply[Done]]) extends Command

  case class Like(userId: UUID, reaction: String, replyTo: ActorRef[StatusReply[Done]]) extends Command

  case class Get(replyTo: ActorRef[State]) extends Command

  //**********************************************************************************************************************
  //  Post Events
  //**********************************************************************************************************************

  sealed trait Event extends CborSerializer

  /**
   * PostCreated Event
   *
   * @param post the post that was created
   */
  final case class Created(post: Posted) extends Event

  /**
   * PostUpdated Event
   *
   * @param body   The string value, used to update the post.
   * @param edited The Instant that the post was update.
   */
  final case class Updated(body: NonEmptyString,  timestamp: Instant) extends Event

  /**
   * PostDeleted Event
   *
   * @param id     Id of the deleted post
   * @param userId Id of the user that is requesting this post be deleted post
   */
  case class Deleted(id: UUID, userId: UUID, timestamp: Instant) extends Event
  /**
   * PostLiked Event
   *
   * @param userId   ID of the user liking said post
   * @param reaction the users reaction
   */
  final case class Liked(userId: UUID, reaction: String, timestamp: Instant) extends Event

  object Event {
    val NumShards = 4
    val tags: Vector[String] = Vector.tabulate(NumShards)(i => s"post-$i")
  }

  sealed trait State extends CborSerializer {
    def applyCommand(cmd: Command)(ctx: Context): ReplyEffect[Event, State]

    def applyEvent(evt: Event): State

    def onGet(replyTo: ActorRef[State]): ReplyEffect[Event, State] =
      Effect.reply(replyTo)(this)

    def onUnhandledEvent(evt: Event): State =
      throw new IllegalStateException(
        s"unexpected event [$evt] in state [${this.getClass.getSimpleName}]"
      )
  }

  object Empty extends State {
    override def applyCommand(cmd: Command)(ctx: Context): ReplyEffect[Event, State] =
      cmd match {
        // Create the post
        case c: Create =>
          val post = Posted(c.postId, c.userId, c.postBody, None, ctx.now(), None)
          Effect.persist(Created(post)).thenReply(c.replyTo)(_ => StatusReply.Ack)
        // Get call
        case c: Get => onGet(c.replyTo)
        // Unhandled commands
        case c: Update => Effect.reply(c.replyTo)(StatusReply.Error(""))
        case c: Delete => Effect.reply(c.replyTo)(StatusReply.Error(""))
        case c: Like => Effect.reply(c.replyTo)(StatusReply.Error(""))
      }

    override def applyEvent(evt: Event): State =
      evt match {
        case Created(post) => post
        case _ => onUnhandledEvent(evt)
      }
  }

  sealed trait NonEmpty extends State {
    def id: UUID

    def userId: UUID

    def postBody: NonEmptyString

    def interactions: Option[String]

    def createdDate: Instant

    def editedDate: Option[Instant]
  }

  case class Posted(
    id: UUID,
    userId: UUID,
    postBody: NonEmptyString,
    interactions: Option[String],
    createdDate: Instant,
    editedDate: Option[Instant]
  ) extends NonEmpty {
    override def applyCommand(cmd: Command)(ctx: Context): ReplyEffect[Event, State] =
      cmd match {
        case c: Update =>
          Effect.persist(Updated(c.body, ctx.now())).thenReply(c.replyTo)(_ => StatusReply.Ack)
        case c: Delete =>
          Effect.persist(Deleted(ctx.entityId, c.userId, ctx.now())).thenReply(c.replyTo)(_ => StatusReply.Ack)
        case c: Like =>
          Effect.persist(Liked(c.userId, c.reaction, ctx.now())).thenReply(c.replyTo)(_ => StatusReply.Ack)
        case c: Get => onGet(c.replyTo)
        case c: Create => Effect.reply(c.replyTo)(StatusReply.Error(""))
      }

    override def applyEvent(evt: Event): State =
      evt match {
        case e: Updated => copy(postBody = e.body, editedDate = Some(e.timestamp))
        case _: Deleted => Empty
        case e: Liked => copy(interactions = Some(e.reaction))
        case _ => onUnhandledEvent(evt)
      }
  }

  case class Context(
    entityId: UUID,
    clock: Clock
  ) {
    def now() = Instant.now(clock)
  }

  def behavior(persistenceId: PersistenceId, context: Context): EventSourcedBehavior[Command, Event, State] =
    EventSourcedBehavior
      .withEnforcedReplies[Command, Event, State](
        persistenceId = persistenceId,
        emptyState = Empty,
        commandHandler = (state, cmd) => state.applyCommand(cmd)(context),
        eventHandler = (state, evt) => state.applyEvent(evt)
      )

  def apply(entityContext: EntityContext[Command], projectionTag: String): Behavior[Command] =
    Behaviors.setup[Command] { _ =>
      behavior(
        PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId),
        Context (
          UUID.fromString(entityContext.entityId),
          Clock.systemUTC()
        )
      )
      //#akka-persistence-behavior-definition
      .withTagger(_ => Set(projectionTag))
      .onPersistFailure(
        SupervisorStrategy.restartWithBackoff(200.millis, 5.seconds, 0.1)
      )
    }
}
