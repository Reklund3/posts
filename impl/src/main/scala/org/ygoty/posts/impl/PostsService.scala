package org.ygoty.posts.impl

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import com.typesafe.config.ConfigFactory
import org.ygoty.posts.impl.actors.PostServiceGuardian
import pureconfig.generic.auto._
import org.ygoty.posts.impl.domain.ServiceConfig
import org.ygoty.posts.impl.entities.Post
import org.ygoty.posts.impl.entities.Post.Event
import org.ygoty.posts.impl.readside.{PostRepository, PostRepositoryImpl}
import pureconfig.ConfigSource
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

import scala.concurrent.ExecutionContextExecutor

object PostsService {
  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load()
    implicit val system: ActorSystem[Nothing] = ActorSystem[Nothing](Behaviors.empty, "PostsService", config)

    val clusterSharding = ClusterSharding(system)

    implicit val ec: ExecutionContextExecutor = system.executionContext

    // Configure postgres readside.
    val dbConfig: DatabaseConfig[PostgresProfile] =
      DatabaseConfig.forConfig("akka.projection.slick", system.settings.config)

    val postRepository = PostRepositoryImpl(dbConfig)

    val serviceConfig = ConfigSource.fromConfig(config.getConfig("service")).loadOrThrow[ServiceConfig]

    new PostsService().run(clusterSharding, postRepository, serviceConfig, system)
  }
}
class PostsService {

  /**
   * Logic to bind the given routes to a HTTP port and add some logging around it
   */
//  def run(clusterSharding: ClusterSharding, system: ActorSystem[_]): Future[Http.ServerBinding] = {
  def run(clusterSharding: ClusterSharding, repository: PostRepository, serviceConfig: ServiceConfig,  system: ActorSystem[_]): Unit = {
    //TODO: Maybe move this somewhere that makes more sense.
    // New Akka typed way of doing the old Persistent Entity Registry
    clusterSharding.init(
      Entity(Post.TypeKey) { ctx =>
        val i = math.abs(ctx.entityId.hashCode % Event.tags.size)
        val selectedTag = Event.tags(i)

        Post(ctx, selectedTag)
      }
    )

    system.systemActorOf(PostServiceGuardian(serviceConfig, repository), "PostServiceActor")
    ()
  }
}
