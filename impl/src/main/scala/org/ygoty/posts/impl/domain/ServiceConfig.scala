package org.ygoty.posts.impl.domain

import pureconfig._
import pureconfig.generic.auto._
import eu.timepit.refined.pureconfig._
import pureconfig.generic.semiauto.deriveConvert

case class ServiceConfig(grpc: GRPCConfig)
object ServiceConfig {
  implicit val convert: ConfigConvert[ServiceConfig] = deriveConvert[ServiceConfig]
}
