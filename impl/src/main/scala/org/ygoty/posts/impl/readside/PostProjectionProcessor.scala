package org.ygoty.posts.impl.readside

import akka.Done
import akka.projection.eventsourced.EventEnvelope
import akka.projection.slick.SlickHandler
import com.typesafe.scalalogging.Logger
import org.ygoty.posts.impl.entities.Post
import slick.dbio.DBIO

import java.util.UUID

class PostProjectionProcessor(repo: PostRepository)
  extends SlickHandler[EventEnvelope[Post.Event]]() {
  import Post._

  private val log = Logger(getClass)

  /**
   * The Envelope handler to process events.
   */
  override def process(envelope: EventEnvelope[Event]): DBIO[Done] = {
    val entityId = envelope.persistenceId
    envelope.event match {
      case e: Created =>
        log.debug(s"$e")
        log.debug(s"Creating post -> ${e.post}")
        repo.createPost(e.post)
      case e: Updated =>
        log.debug(s"$e")
        repo.updatePost(UUID.fromString(entityId), e.body.value, e.timestamp)
      case e: Deleted =>
        log.debug(s"$e")
        repo.deletePost(UUID.fromString(entityId))
      case e: Liked   =>
        log.debug(s"$e")
        DBIO.successful(Done)
    }
  }

}
