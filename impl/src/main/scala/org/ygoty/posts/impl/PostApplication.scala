package org.ygoty.posts.impl

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.grpc.ServiceDescription
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import com.softwaremill.macwire.wire
import eu.timepit.refined.types.all.PortNumber
import org.ygoty.posts.impl.components.{KafkaProjection, ReadsideProjection}
import org.ygoty.posts.impl.domain.ServiceConfig
import org.ygoty.posts.impl.entities.Post
import org.ygoty.posts.impl.grpc.{PostService, PostServiceHandler}
import org.ygoty.posts.impl.readside.{PostProjectionProcessor, PostRepository, PostRepositoryImpl}
import pureconfig.ConfigSource
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile


class PostApplication(config: ServiceConfig, repository: PostRepository, system: ActorSystem[_])
  extends AkkaGrpcApplication(system) {

  override val serviceConfig: ServiceConfig = config
//  override val grpcInterface: String = "127.0.0.1"
//  override val grpcPort: Int = 8080

  lazy val clusterSharding: ClusterSharding = ClusterSharding(system)

  lazy val postRepository: PostRepository = wire[PostRepositoryImpl]

  override def grpcServiceDescription: ServiceDescription = PostService

  override lazy val grpcService: HttpRequest => scala.concurrent.Future[HttpResponse] =
    PostServiceHandler(new PostServiceImpl(clusterSharding, repository, system))(system)

  // TODO: we should really derive a pureconfig loader for this type instead of leaning on dynamic loading
  val dbConfig: DatabaseConfig[PostgresProfile] =
    DatabaseConfig.forConfig("akka.projection.slick", system.settings.config)

  // TODO: should we pair this with the DatabaseConfig in a Provider type class
  //  similar to `com.lightbend.lagom.internal.persistence.jdbc.SlickDbProvider`?
  CoordinatedShutdown(system).addTask(
    CoordinatedShutdown.PhaseBeforeActorSystemTerminate,
    "shutdown-slick-db"
  ) { () =>
    dbConfig.db.shutdown.map(_ => Done)(system.executionContext)
  }

  // start clusterSharding entities
//  Post.initSharding(system)

  // start cluster projections
  // Create the base service.proto tables if they don't exist, there is a better way to do this!!!
  dbConfig.db.run(postRepository.createTables())

  val readsideHandler = new PostProjectionProcessor(postRepository)

  ReadsideProjection[Post.Event](
    "Post",
    dbConfig,
    readsideHandler,
    Post.Event.tags
  )(system)

  // Kafka producer
  val topicName = "posts"

  KafkaProjection(
    s"kafka-$topicName",
    dbConfig,
    Post.Event.tags
  )(system)

}
