package org.ygoty.posts.impl

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.{ConnectionContext, Http, HttpsConnectionContext}
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.pki.pem.{DERPrivateKeyLoader, PEMDecoder}
import eu.timepit.refined.types.net.PortNumber
import org.ygoty.posts.impl.domain.ServiceConfig

import java.security.{KeyStore, SecureRandom}
import java.security.cert.{Certificate, CertificateFactory}
import javax.net.ssl.{KeyManagerFactory, SSLContext}
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

abstract class AkkaGrpcApplication(system: ActorSystem[_]) {
  implicit protected val sys: ActorSystem[_] = system
  implicit protected val ec: ExecutionContext = system.executionContext

  def serviceConfig: ServiceConfig

  def grpcServiceDescription: akka.grpc.ServiceDescription

  def grpcService: HttpRequest => scala.concurrent.Future[HttpResponse]

  def start(): Future[Http.ServerBinding] = {
    val serviceHandler: HttpRequest => Future[HttpResponse] = grpcService

    Http()
      .newServerAt(serviceConfig.grpc.interface, serviceConfig.grpc.port.value)
      //todo: Add this back
//        .enableHttps(serverHttpContext)
      .bind(serviceHandler)
      .map(_.addToCoordinatedShutdown(5.seconds))
  }


  private def serverHttpContext: HttpsConnectionContext = {
    val privateKey =
      DERPrivateKeyLoader.load(PEMDecoder.decode(readPrivateKeyPem()))
    val fact = CertificateFactory.getInstance("X.509")
    val cer = fact.generateCertificate(
      classOf[PostsService].getResourceAsStream("/certs/server1.pem")
    )
    val ks = KeyStore.getInstance("PKCS12")
    ks.load(null)
    ks.setKeyEntry(
      "private",
      privateKey,
      new Array[Char](0),
      Array[Certificate](cer)
    )
    val keyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, null)
    val context = SSLContext.getInstance("TLS")
    context.init(keyManagerFactory.getKeyManagers, null, new SecureRandom)
    ConnectionContext.httpsServer(context)
  }

  private def readPrivateKeyPem(): String =
    Source.fromResource("certs/server1.key").mkString
}
