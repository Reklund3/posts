package org.ygoty.posts.impl.readside

import akka.Done
import akka.kafka.scaladsl.SendProducer
import akka.projection.eventsourced.EventEnvelope
import akka.projection.slick.SlickHandler
import com.google.protobuf.timestamp.Timestamp
import com.typesafe.scalalogging.Logger
import org.apache.kafka.clients.producer.ProducerRecord
import org.ygoty.posts.impl.entities.Post
import slick.dbio.DBIO

import scala.concurrent.ExecutionContext

//class PostKafkaProjection(topic: String, sendProducer: SendProducer[String, Post.Event])(implicit ec: ExecutionContext)
class PostKafkaProcessor(
                          topic: String,
                          sendProducer: SendProducer[String, Array[Byte]]
                        )(implicit ec: ExecutionContext)
  extends SlickHandler[EventEnvelope[Post.Event]]() {
  import Post._

  private val log = Logger(getClass)

  /**
   * The Envelope handler to process events.
   */
  override def process(envelope: EventEnvelope[Event]): DBIO[Done] = {
    val entityId = envelope.persistenceId
    val record = new ProducerRecord(topic, entityId, serialize(entityId, envelope.event))
    sendProducer.send(record).map { recordMetadata =>
      log.info(
        "Published event [{}] to topic/partition {}/{}",
        envelope.event,
        topic,
        recordMetadata.partition
      )
    }
    DBIO.successful(Done)
  }

  import org.ygoty.posts.impl.grpc
  private[readside] def serialize(id: String, event: Post.Event): Array[Byte] =  {
    val protoSealedValue = event match {
      case e: Created =>
        grpc.PostEvent.SealedValue.Created(
          grpc.PostCreated(
            Some(
              grpc.Post(
                id = id,
                userId = e.post.userId.toString,
                body = e.post.postBody.value,
                createdDate = Some(Timestamp(e.post.createdDate))
              )
            )
          )
        )
      case e: Updated =>
        grpc.PostEvent.SealedValue.Updated(
          grpc.PostUpdated(
            id = id,
            body = e.body.value,
            editedAt = Some(Timestamp(e.timestamp))
          )
        )
      case e: Deleted =>
        grpc.PostEvent.SealedValue.Deleted(
          grpc.PostDeleted(
            id = id,
            deleterId = e.userId.toString,
            deletedAt = Some(Timestamp(e.timestamp))
          )
        )
      case e: Liked =>
        grpc.PostEvent.SealedValue.Liked(
          grpc.PostLiked(
            id = id,
            userId = e.userId.toString,
            reaction = e.reaction,
            likedAt = Some(Timestamp(e.timestamp))
          )
        )
    }
    grpc.PostEvent.of(protoSealedValue).toByteArray
  }

}
