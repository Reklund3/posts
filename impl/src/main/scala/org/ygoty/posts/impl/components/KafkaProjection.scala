package org.ygoty.posts.impl.components

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ShardedDaemonProcess
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.SendProducer
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.Offset
import akka.projection.eventsourced.EventEnvelope
import akka.projection.eventsourced.scaladsl.EventSourcedProvider
import akka.projection.scaladsl.{ExactlyOnceProjection, SourceProvider}
import akka.projection.slick.{SlickHandler, SlickProjection}
import akka.projection.{ProjectionBehavior, ProjectionId}
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}
import org.ygoty.posts.impl.readside.PostKafkaProcessor
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

object KafkaProjection {
  private def sourceProvider[Event](tag: String, system: ActorSystem[_]): SourceProvider[Offset, EventEnvelope[Event]] =
    EventSourcedProvider
      .eventsByTag[Event](
        system = system,
        readJournalPluginId = CassandraReadJournal.Identifier,
        tag = tag
      )

  private def projection[Event](
    projectionName: String,
    dbConfig: DatabaseConfig[PostgresProfile],
    handler: SlickHandler[EventEnvelope[Event]],
    tag: String
  )(
    implicit actorSystem: ActorSystem[_]
  ): ExactlyOnceProjection[Offset, EventEnvelope[Event]] = {
    val projectionId = ProjectionId(projectionName, tag)
    SlickProjection
      .exactlyOnce(
        projectionId,
        sourceProvider(tag, actorSystem),
        dbConfig,
        handler = () => handler
      )
  }

  def apply(
    projectionName: String,
    dbConfig: DatabaseConfig[PostgresProfile],
    tags: Vector[String]
  )(implicit actorSystem: ActorSystem[_]) = {
    import actorSystem.executionContext

    val bootstrapServers = actorSystem.settings.config.getString("service.kafka.bootstrap-server")
    val topicName = "posts"
    val producerSettings =
      ProducerSettings(actorSystem, new StringSerializer, new ByteArraySerializer)
        .withBootstrapServers(bootstrapServers)
    import akka.actor.typed.scaladsl.adapter._ // FIXME might not be needed in later Alpakka Kafka version?

    val sendProducer = SendProducer(producerSettings)(actorSystem.toClassic)
    val handler = new PostKafkaProcessor(topicName, sendProducer)

    //Dangerous!!!! Remove for production or all will burn.
//    SlickProjection
//      .dropTablesIfExists(dbConfig)
    // Setup the offset table
    SlickProjection
      .createTablesIfNotExists(dbConfig)

    // Setup the readside projection
    ShardedDaemonProcess(actorSystem)
      .init[ProjectionBehavior.Command](
        name = projectionName.toLowerCase(),
        numberOfInstances = tags.size,
        behaviorFactory = (i: Int) => ProjectionBehavior(
          projection(projectionName, dbConfig, handler, tags(i))
        ),
        stopMessage = ProjectionBehavior.Stop
      )
  }

}
