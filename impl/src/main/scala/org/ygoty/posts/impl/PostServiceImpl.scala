package org.ygoty.posts.impl

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.pattern.StatusReply
import akka.util.Timeout
import com.google.protobuf.timestamp.Timestamp
import eu.timepit.refined.types.string.NonEmptyString
import org.ygoty.posts.impl.components.ClusterShardingExtensions
import org.ygoty.posts.impl.grpc._
import org.ygoty.posts.impl.readside.PostRepository

import java.time.Instant
import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

class PostServiceImpl(
  val clusterSharding: ClusterSharding,
  readside: PostRepository,
  system: ActorSystem[_]
) extends PostService with ClusterShardingExtensions {

  implicit val askTimeout: Timeout = Timeout(5.seconds)
  implicit val ec: ExecutionContext = system.executionContext

  override def getPost(in: GetPostRequest): Future[GetPostResponse] = {
    readside
      .getPostById(UUID.fromString(in.id))
      .map {
        case Some(post) =>
          GetPostResponse(
            GetPostResponse.Response.Post(post)
          )
        case None =>
          GetPostResponse(
            GetPostResponse.Response.NotFound(PostNotFound(404, s"Post with id ${in.id} not found."))
          )
      }
  }

  override def listPosts(in: ListPostsRequest): Future[ListPostsResponse] =
    readside
      .getPosts(in.pageNumber, in.pageSize)
      .map(posts => ListPostsResponse(posts))

  override def createPost(request: CreatePostRequest): Future[CreatePostResponse] = {
    val newPostId = UUID.randomUUID()
    NonEmptyString.from(request.body) match {
      case Right(body) =>
        postRef(newPostId.toString)
          .ask[StatusReply[Done]](replyTo =>
            entities.Post.Create(newPostId, UUID.fromString(request.userId), body, replyTo)
          )
          .map {
            case StatusReply.Ack =>
              CreatePostResponse(
                CreatePostResponse.Response.Success(
                  CreatePostSuccess(
                    Some(
                      Post(
                        id = newPostId.toString,
                        userId = request.userId,
                        body = request.body,
                        createdDate = Some(Timestamp(Instant.now()))
                      )
                    )
                  )
                )
              )
            case StatusReply.Error(msg) => CreatePostResponse(
              CreatePostResponse.Response.Failure(
                CreatePostFailure(
                  Seq(s"Failed to create post due to: $msg")
                )
              )
            )
          }
      case Left(_) =>
        Future.successful(
          CreatePostResponse(
            CreatePostResponse.Response.Failure(
              CreatePostFailure(
                Seq(s"The post must contain a body.")
              )
            )
          )
        )
    }
  }

  override def updatePost(request: UpdatePostRequest): Future[UpdatePostResponse] = {
    NonEmptyString.from(request.body) match {
      case Right(postBody) =>
        postRef(request.postId)
          .ask[StatusReply[Done]](replyTo => entities.Post.Update(postBody, replyTo))
          .map {
            case StatusReply.Ack => UpdatePostResponse(
              UpdatePostResponse.Response.Success(UpdatePostSuccess())
            )
            case StatusReply.Error(msg) => UpdatePostResponse(
              UpdatePostResponse.Response.Failure(
                UpdatePostFailure(
                  Seq(s"Failed to update the post due to: $msg")
                )
              )
            )
          }
      case Left(_) =>
        Future.successful(
          UpdatePostResponse(
            UpdatePostResponse.Response.Failure(
              UpdatePostFailure(
                Seq(s"Post body cannot be empty.")
              )
            )
          )
        )
    }

  }

  override def deletePost(request: DeletePostRequest): Future[DeletePostResponse] = {
    postRef(request.postId)
      .ask[StatusReply[Done]](replyTo => entities.Post.Delete(UUID.fromString(request.userId), replyTo))
      .map {
        case StatusReply.Ack => DeletePostResponse(
          DeletePostResponse.Response.Success(DeletePostSuccess())
        )
        case StatusReply.Error(msg) => DeletePostResponse(
          DeletePostResponse.Response.Failure(
            DeletePostFailure(
              Seq(s"Failed to delete post due to: $msg")
            )
          )
        )
      }
  }

  override def likePost(request: LikePostRequest): Future[LikePostResponse] = {
    postRef(request.postId)
      .ask[StatusReply[Done]](replyTo => entities.Post.Like(UUID.fromString(request.userId), "", replyTo))
      .map {
        case StatusReply.Ack => LikePostResponse(
          LikePostResponse.Response.Success(
            LikePostSuccess()
          )
        )
        case StatusReply.Error(msg) => LikePostResponse(
          LikePostResponse.Response.Failure(
            LikePostFailure(
              Seq(s"Failed to like post due to: $msg")
            )
          )
        )
      }
  }
}
