package org.ygoty.posts.impl.domain

import eu.timepit.refined.types.all.PortNumber
import eu.timepit.refined.pureconfig._
import pureconfig.ConfigConvert
import pureconfig.generic.semiauto.deriveConvert

case class GRPCConfig(interface: String, port: PortNumber)
object GRPfCConfig {
  implicit val convert: ConfigConvert[GRPCConfig] = deriveConvert[GRPCConfig]
}