package org.ygoty.posts.impl.readside

import akka.Done
import org.ygoty.posts.impl.entities
import org.ygoty.posts.impl.grpc.Post
import slick.basic.DatabaseConfig
import slick.dbio.{DBIO, Effect, NoStream}
import slick.jdbc.PostgresProfile
import slick.sql.FixedSqlAction

import java.time.Instant
import java.util.UUID
import scala.concurrent.Future

trait PostRepository {
	def database: DatabaseConfig[PostgresProfile]

	def createTables(): FixedSqlAction[Unit, NoStream, Effect.Schema]

	/** Get a post by its id.
	 *
	 * @param id The UUID of the post.
	 * @return The Optional post.
	 */
	def getPostById(id: UUID): Future[Option[Post]]

	/** Get a list of posts
	 *
	 * @param pageSize  The number of posts to fetch.
	 * @return					The list of posts.
	 */
	def getPosts(pageNumber: Int, pageSize: Int): Future[Seq[Post]]

	/** Inserts the post into the Posts Table
	 *
	 * @param post   The post to be interested into the Post table
	 * @return
	 */
	def createPost(post: entities.Post.NonEmpty): DBIO[Done]

	/** Updates a post in the Posts Table
	 *
	 * @param postId  The updated post to be replace the post that is in the table
	 * @return
	 */
	def updatePost(postId: UUID, postBody: String, edited: Instant): DBIO[Done]

	/** Delete a post from the Posts Table
	 *
	 * @param postId
	 * @return
	 */
	def deletePost(postId: UUID): DBIO[Done]
}