package org.ygoty.posts.impl.actors

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.ClusterEvent
import akka.cluster.typed.{Cluster, SelfRemoved, SelfUp, Subscribe}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri
import akka.management.cluster.bootstrap.ClusterBootstrap
import akka.management.scaladsl.AkkaManagement
import org.ygoty.posts.impl.PostApplication
import org.ygoty.posts.impl.domain.ServiceConfig
import org.ygoty.posts.impl.readside.PostRepository

import scala.concurrent.duration.DurationInt

object PostServiceGuardian {
  sealed trait Message
  sealed trait Command extends Message

  sealed trait InternalMessage extends Message

  final case class AkkaManagementStarted(address: Uri) extends InternalMessage
  final case class AkkaManagementStartFailed(cause: Throwable) extends InternalMessage

  // TODO: Do we want to turn this into a wrapper so that we can describe the cluster state?
  //  (see [[akka.cluster.typed.SelfUp]] and [[akka.cluster.ClusterEvent.CurrentClusterState]]
  case object NodeMemberUp extends InternalMessage
  case object NodeMemberDown extends InternalMessage
  final case class ClusterMemberEvent(event: ClusterEvent.MemberEvent) extends InternalMessage
  final case class ServerBound(binding: Http.ServerBinding) extends InternalMessage
  final case class BindingFailed(reason: Throwable) extends InternalMessage

  def apply(applicationConfig: ServiceConfig, repository: PostRepository): Behavior[Command] =
    behavior(applicationConfig, repository).narrow[Command]

  def behavior(applicationConfig: ServiceConfig, repository: PostRepository): Behavior[Message] = init(applicationConfig, repository)

  private[actors] def init(applicationConfig: ServiceConfig, repository: PostRepository): Behavior[Message] =
    Behaviors.setup { ctx =>
      val cluster = Cluster(ctx.system)
      ctx
        .log
        .info(s"Started [{}], cluster.selfAddress = {}", ctx.system, cluster.selfMember.address)
      val selfUpAdapter = ctx.messageAdapter[SelfUp](_ => NodeMemberUp)
      val selfRemovedAdapter = ctx.messageAdapter[SelfRemoved](_ => NodeMemberDown)
      val clusterMemberEventAdapter = ctx.messageAdapter(ClusterMemberEvent)
      // subscribe to cluster membership events so that we know when
      // - we are marked as up,
      // - we are marked as removed
      // - other nodes join/leave the cluster
      cluster.subscriptions ! Subscribe(selfUpAdapter, classOf[SelfUp])
      cluster.subscriptions ! Subscribe(selfRemovedAdapter, classOf[SelfRemoved])
      cluster.subscriptions ! Subscribe(
        clusterMemberEventAdapter,
        classOf[ClusterEvent.MemberEvent]
      )

      // Start Akka Management so that we can discover and be discovered by other nodes in the cluster
      // Akka Management hosts the HTTP routes used by cluster bootstrap
      ctx.pipeToSelf(AkkaManagement(ctx.system).start()) {
        _.fold(AkkaManagementStartFailed, AkkaManagementStarted)
      }
      startingAkkaManagement(applicationConfig, repository)
    }

  def startingAkkaManagement(applicationConfig: ServiceConfig, repository: PostRepository): Behavior[Message] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case x: AkkaManagementStarted =>
          ctx.log.info(s"Akka Management has been started on ${x.address}")
          // Starting the bootstrap process needs to be done explicitly after management
          ClusterBootstrap(ctx.system).start()
          startingClusterBootstrap(applicationConfig, repository)

        case x: AkkaManagementStartFailed =>
          ctx.log.error(s"Akka Management failed to start!", x.cause)
          Behaviors.stopped
        // we only expect to be handling akka management start results here
        case other =>
          ctx.log.warn(s"Unexpected messages in 'startingAkkaManagement': {}", other)
          Behaviors.unhandled
      }
    }

  def startingClusterBootstrap(applicationConfig: ServiceConfig, repository: PostRepository): Behavior[Message] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        // we have joined the cluster
        case NodeMemberUp =>
          ctx.log.info("Joined the cluster!")
          val application = new PostApplication(applicationConfig, repository, ctx.system)
          ctx.pipeToSelf(application.start()) {
            _.fold(BindingFailed, ServerBound)
          }
          startingServer()
        // we have been removed from the cluster
        case NodeMemberDown =>
          ctx.log.info("Removed from the cluster!")
          Behaviors.stopped
        case x: ClusterMemberEvent =>
          ctx.log.info("Cluster MemberEvent: {}", x.event)
          Behaviors.same
        // we only expect to be handling akka management start results here
        case other =>
          ctx.log.warn(s"Unexpected messages in 'startingClusterBootstrap': {}", other)
          Behaviors.unhandled
      }
    }

  def startingServer(): Behavior[Message] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case x: ServerBound =>
        val address = x.binding.localAddress
        // TODO: use serviceName here?
        ctx
          .log
          .info(
            "PostsHttpServer online at http://{}:{}/",
            address.getHostString,
            address.getPort
          )
        val shutdown = CoordinatedShutdown(ctx.system)
        shutdown.addTask(
          CoordinatedShutdown.PhaseServiceRequestsDone,
          "http-graceful-terminate"
        ) { () =>
          x.binding.terminate(10.seconds).map { _ =>
            ctx.log.info(
              "PostsHttpServer http://{}:{}/ graceful shutdown completed",
              address.getHostString,
              address.getPort
            )
            Done
          }(ctx.system.executionContext)
        }
        running(x.binding)
      case x: BindingFailed =>
        ctx.log.error("Failed to bind grpc server", x.reason)
        Behaviors.stopped
      case x: ClusterMemberEvent =>
        ctx.log.info("Cluster MemberEvent: {}", x.event)
        Behaviors.same
      // we only expect to be handling akka management start results here
      case other =>
        ctx.log.warn(s"Unexpected messages in 'startingServer': {}", other)
        Behaviors.unhandled
    }

  }

  def running(
  // TODO: do we need to hold onto the binding to unbind in specific situations?
     binding: Http.ServerBinding
  ): Behavior[Message] =
    Behaviors.receive { (ctx, msg) =>
        msg match {
          case x: ClusterMemberEvent =>
            ctx.log.info("Cluster MemberEvent: {}", x.event)
            Behaviors.same
          // we only expect to be handling akka management start results here
          case other =>
            ctx.log.warn(s"Unexpected messages in 'running': {}", other)
            Behaviors.unhandled
        }
      }
  // TODO: handle cleaning up the server binding on known failures?
//        .receiveSignal {
//          case (ctx, Terminated(value)) if value == ctx.self =>
//            binding.terminate(FiniteDuration(10, TimeUnit.SECONDS))
//            Behaviors.stopped
//        }
}
