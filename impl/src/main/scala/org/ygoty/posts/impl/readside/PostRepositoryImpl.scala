package org.ygoty.posts.impl.readside

import akka.Done
import com.google.protobuf.timestamp.Timestamp
import org.ygoty.posts.impl.entities
import org.ygoty.posts.impl.grpc.Post
import slick.basic.DatabaseConfig
import slick.dbio.Effect
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._
import slick.sql.FixedSqlAction

import java.time.Instant
import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

case class PostRepositoryImpl(database: DatabaseConfig[PostgresProfile])(implicit ec: ExecutionContext) extends PostRepository {

  case class PostRow(
    postId: UUID,
    userId: UUID,
    postBody: String,
    createdDate: Instant,
    editedDate: Option[Instant],
  )

  class PostsTable(tag: Tag) extends Table[PostRow](tag, "posts") {
    def postId = column[UUID]("post_id", O.PrimaryKey)

    def userId = column[UUID]("user_id")

    def postBody = column[String]("post_body")

    //		def interactions = column[Seq[UserInteraction]]("user_interactions")(interactionConverter)
    def createdDate = column[Instant]("created_date")

    def editedDate = column[Option[Instant]]("edited_date")

    def * = (postId, userId, postBody, createdDate, editedDate) <> ((PostRow.apply _).tupled, PostRow.unapply)
  }

  private val Posts = TableQuery[PostsTable]

  def createTables(): FixedSqlAction[Unit, NoStream, Effect.Schema] = Posts.schema.createIfNotExists

  def getPostById(id: UUID): Future[Option[Post]] =
    database.db.run(getPostByIdQuery(id)).map(row => row.map(convertPostRowToApi))

  private def getPostByIdQuery(id: UUID): DBIO[Option[PostRow]] =
    Posts
      .filter(_.postId === id)
      .result
      .headOption

  //TODO: the defaults should be configured values passed to here.
  override def getPosts(pageNumber: Int = 0, pageSize: Int = 25): Future[Seq[Post]] = {
    database
      .db.run(
        Posts.drop(pageNumber*pageSize).take(pageSize).result.map(_.map(convertPostRowToApi))
      )
  }

  /** Inserts the post into the Posts Table
   *
   * @param post The post to be interested into the Post table
   * @return
   */
  def createPost(post: entities.Post.NonEmpty): DBIO[Done] = {
    Posts
      .insertOrUpdate(PostRow(post.id, post.userId, post.postBody.value, post.createdDate, post.editedDate))
      .map(_ => Done)
  }

  /** Updates a post in the Posts Table
   *
   * @param postId The updated post to be replace the post that is in the table
   * @return
   */
  def updatePost(postId: UUID, postBody: String, edited: Instant): DBIO[Done] = {
    Posts
      .filter(_.postId === postId)
      .result
      .headOption
      .flatMap {
        case Some(post) => Posts.insertOrUpdate(post.copy(postBody = postBody, editedDate = Some(edited)))
        case None => throw new RuntimeException(s"Didn't find post with postId: $postId")
      }
      .map(_ => Done)
      .transactionally
  }

  /** Delete a post from the Posts Table
   *
   * @param postId
   * @return
   */
  def deletePost(postId: UUID) = {
    Posts.filter(_.postId === postId)
      .delete
      .map(_ => Done)
      .transactionally
  }

  implicit def convertPostRowToApi(post: PostRow): Post = {
    Post(
      id = post.postId.toString,
      userId = post.userId.toString,
      body = post.postBody,
      createdDate = Some(Timestamp(post.createdDate)),
      //			updatedDate = post.editedDate
    )
  }
}
