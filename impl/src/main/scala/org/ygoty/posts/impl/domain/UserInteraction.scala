package org.ygoty.posts.impl.domain

import org.ygoty.posts.impl.entities.Post.CborSerializer

import java.util.UUID

case class UserInteraction(
                            userId: UUID,
                            //TODO: Look into a lib that uses the standard emoji's and see how they store them. Need to update this to that lib.
                            interaction: String
                          ) extends CborSerializer
