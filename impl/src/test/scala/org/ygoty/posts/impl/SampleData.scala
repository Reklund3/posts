package org.ygoty.posts.impl

import eu.timepit.refined.types.all.NonEmptyString
import org.ygoty.posts.impl.domain.User
import org.ygoty.posts.impl.grpc.Post
import org.ygoty.posts.impl.entities.Post.Empty
import org.ygoty.posts.impl.entities.Post.Posted

import java.time.Instant
import java.util.UUID

object SampleData {

  final val PostId = UUID.fromString("15C42E80-A659-4B4F-8541-2E4033C56876")
  final val UserId = UUID.fromString("7D23AA96-8EC5-4BD9-8AAC-99824A4DDEC7")
  final val TestBody = NonEmptyString.unsafeFrom("Test post body")
  final val TestUpdatedBody = NonEmptyString.unsafeFrom("Test updated post body")
  final val CreatedTime = Instant.now
  final val UpdatedTime = CreatedTime.plusNanos(1000)

  final val TestUser = User(UUID.randomUUID(), "testUser", "testUser@email")

  final val TestEmptyPost = Empty
  //TODO: Need to implement test with interactions as well as empty.
  final val TestCreatedPost = Posted(PostId, UserId, TestBody, None, CreatedTime, None)
  final val TestApiCreatedPost = Post(PostId.toString, UserId.toString, TestBody.value/*, CreatedTime, None*/)

  final val TestUpdatedPost = TestCreatedPost.copy(postBody = TestUpdatedBody, editedDate = Some(UpdatedTime))

}
