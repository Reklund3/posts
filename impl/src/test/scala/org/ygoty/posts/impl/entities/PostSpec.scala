package org.ygoty.posts.impl.entities

import akka.Done
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.pattern.StatusReply
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit.SerializationSettings
import akka.persistence.typed.PersistenceId
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funspec.AnyFunSpecLike
import org.scalatest.matchers.should.Matchers
import org.ygoty.posts.impl.SampleData

import java.time.{Clock, Instant, ZoneId}
import java.util.UUID

class PostSpec
  extends ScalaTestWithActorTestKit(
    ConfigFactory.parseString(PostSpec.config)
      .withFallback(EventSourcedBehaviorTestKit.config)
  )
  with AnyFunSpecLike
  with Matchers
  with BeforeAndAfterEach {

  import Post._
  import SampleData._

  val fixedInstant = Instant.now()
  val fixedClock: Clock = Clock.fixed(fixedInstant, ZoneId.systemDefault())

  describe("Post") {
    describe("[Empty]") {
      describe("Get =>") {
        it("should handle Get") {
          pending
        }
      }
      describe("CreatePost =>") {
        it("should handle Create") {
          // Setup Test
          val entityId = UUID.randomUUID()
          val post = EventSourcedBehaviorTestKit[Command, Event, State](
            testKit.system,
            behavior(PersistenceId(Post.TypeKey.name, entityId.toString), Context(entityId, fixedClock)),
            SerializationSettings.enabled
          )
          val cmd = Create(TestCreatedPost.id, TestCreatedPost.userId, TestCreatedPost.postBody, _)

          val result = post.runCommand[StatusReply[Done]](cmd)
          result.reply shouldBe StatusReply.Ack
          result.event shouldBe Created(TestCreatedPost.copy(createdDate = fixedInstant))
          result.state shouldBe TestCreatedPost.copy(createdDate = fixedInstant)

          // Test Get call
          val stateProbe = createTestProbe[State]()
          post.runCommand(Get(stateProbe.ref))
          val expectedState = stateProbe.expectMessageType[State]

          expectedState shouldBe TestCreatedPost.copy(createdDate = fixedInstant)
        }
      }
      describe("Update =>") {
        it("should handle Update") {
          pending
        }
      }
      describe("Delete =>") {
        it("should handle Delete") {
          pending
        }
      }
      describe("Like =>") {
        it("should handle Like") {
          pending
        }
      }
    }
    it("should postUpdated (Command)") {
      // Setup Test
      val entityId = UUID.randomUUID()
      val post = EventSourcedBehaviorTestKit[Command, Event, State](
        system,
        behavior(PersistenceId(Post.TypeKey.name, entityId.toString), Context(entityId, fixedClock))
      )
      val cmd = Create(TestCreatedPost.id, TestCreatedPost.userId, TestCreatedPost.postBody, _)

      val result = post.runCommand[StatusReply[Done]](cmd)

      // Check created
      result.reply shouldBe StatusReply.Ack
      result.event shouldBe Created(TestCreatedPost.copy(createdDate = fixedInstant))
      result.state shouldBe TestCreatedPost.copy(createdDate = fixedInstant)

      val cmd2 = Update(TestUpdatedBody, _)

      val result2 = post.runCommand(cmd2)
      result2.reply shouldBe StatusReply.Ack
      result2.event shouldBe Updated(TestUpdatedBody, fixedInstant)
      result2.state shouldBe TestUpdatedPost.copy(createdDate = fixedInstant, editedDate = Some(fixedInstant))
    }
  }

}

object PostSpec {
  val config =
    """akka.loglevel="INFO"
      |akka.actor.serialization-bindings {
			|  "org.ygoty.posts.impl.entities.Post$CborSerializer" = jackson-cbor
			|}
			|""".stripMargin
}