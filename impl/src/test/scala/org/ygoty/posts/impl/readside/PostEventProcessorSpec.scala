package org.ygoty.posts.impl.readside

import akka.persistence.query.Sequence
import akka.projection.eventsourced.EventEnvelope
import com.dimafeng.testcontainers.{ForAllTestContainer, PostgreSQLContainer}
import com.softwaremill.macwire.wire
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, OptionValues}
import org.scalatest.funspec.AsyncFunSpec
import org.scalatest.matchers.should.Matchers
import org.ygoty.posts.impl.entities.Post
import org.ygoty.posts.impl.SampleData

import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.jdk.CollectionConverters._

class PostEventProcessorSpec
  extends AsyncFunSpec
    with Matchers
    with BeforeAndAfterAll
    with OptionValues {
//    with ForAllTestContainer {
  import Post._
  import PostEventProcessorSpec._
  import SampleData._

//  val container: PostgreSQLContainer = PostgreSQLContainer(
//    databaseName = "post",
//    username = PostgresqlUser,
//    password = PostgresqlPass,
//    mountPostgresDataToTmpfs = true
//  )

//  trait TestPostComponents
//    extends PostComponents
//    with AhcWSComponents
//    with TestTopicComponents
//    with TestReadSideComponents
//    with HikariCPComponents
//    with LocalServiceLocator
//
//  trait TestApplicationConfiguration extends LagomApplication {
//    self: TestPostComponents =>
//
//    override def additionalConfiguration: AdditionalConfiguration =
//      super.additionalConfiguration ++ ConfigFactory.parseMap(
//        Map(
//          "db.default.url" -> s"${container.jdbcUrl}",
//          "db.default.username" -> s"$PostgresqlUser",
//          "db.default.password" -> s"$PostgresqlPass",
//          "cassandra-query-journal.eventual-consistency-delay" -> "0"
//        ).asJava
//      )
//
//    override lazy val lagomServer = serverFor[PostService](wire[PostServiceImplOld])
//
//  }
//
//  type TestApplication =
//    LagomApplication with TestPostComponents with TestApplicationConfiguration
//
//  val applicationLoader: LagomApplicationContext => TestApplication =
//    (ctx: LagomApplicationContext) =>
//      new LagomApplication(ctx) with TestPostComponents with TestApplicationConfiguration
//
//  var server: ServiceTest.TestServer[TestApplication] = _
//  val offset = new AtomicInteger()
//
//  override def beforeAll(): Unit =
//    server = ServiceTest.startServer[TestApplication](ServiceTest.defaultSetup.withCassandra(true))(
//      applicationLoader
//    )
//
//  override def afterAll(): Unit = server.stop()

  describe("PostEventProcessorSpec") {
    describe("Readside") {
      it("should insert a post into the readside") {
//        val id = UUID.randomUUID()
//        for {
//          _ <- server.application.readSide.feed[EventEnvelope[Event]](
//            id.toString,
//            Created(TestCreatedPost.copy(id = id)),
//            Sequence(offset.getAndIncrement.toLong)
//          )
//          post <- server.application.postRepository.getPostById(id)
//        } yield {
//          post should ===(Some(TestCreatedPost.copy(id = id)))
//        }
        pending
      }
      it("should update a post in the readside") {
//        val id = UUID.randomUUID()
//        for {
//          _ <- server.application.readSide.feed[Event](
//            id.toString,
//            Created(TestCreatedPost.copy(id = id)),
//            Sequence(offset.getAndIncrement.toLong)
//          )
//          _ <- server.application.readSide.feed[Event](
//            id.toString,
//            Updated(TestUpdatedBody, UpdatedTime),
//            Sequence(offset.getAndIncrement.toLong)
//          )
//          post <- server.application.postRepository.getPostById(id)
//        } yield {
//          post should ===(Some(TestApiCreatedPost.copy(id = id, body = TestUpdatedBody, updatedDate = Some(UpdatedTime))))
//        }
        pending
      }
      it("should delete a post from the readside") {
//        val id = UUID.randomUUID()
//        for {
//          _ <- server.application.readSide.feed[Event](
//            id.toString,
//            Created(TestCreatedPost.copy(id = id)),
//            Sequence(offset.getAndIncrement.toLong)
//          )
//          _ <- server.application.readSide.feed[Event](
//            id.toString,
//            Deleted(id),
//            Sequence(offset.getAndIncrement.toLong)
//          )
//          post <- server.application.postRepository.getPostById(id)
//        } yield {
//          post should ===(None)
//        }
        pending
      }
      it("should like a post in the readside") {
        pending
      }
    }
  }

}
object PostEventProcessorSpec {
  final val PostgresqlUser = "testUser"
  final val PostgresqlPass = "testPass"
}
