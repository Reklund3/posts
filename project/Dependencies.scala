import sbt._

object Dependencies {

  private object akka {
    val group = "com.typesafe.akka"
    val version = "2.6.20"

    val actor = group %% "akka-actor" % version
    val actorTyped = group %% "akka-actor-typed" % version
    val clusterTyped = group %% "akka-cluster-typed" % version
    val clusterShardingTyped = group %% "akka-cluster-sharding-typed" % version
    val discovery = group %% "akka-discovery" % version
    val jackson = group %% "akka-serialization-jackson" % version
    val persistenceCassandra = group %% "akka-persistence-cassandra" % "1.0.6"
    val persistenceQuery = group %% "akka-persistence-query" % version
    val persistenceTyped = group %% "akka-persistence-typed" % version
    val persistenceTestKit = group %% "akka-persistence-testkit" % version % Test
    val streamTestKit = group %% "akka-stream-testkit" % version % Test
    val testKit = group %% "akka-testkit" % version % Test
    val testKitTyped = group %% "akka-actor-testkit-typed" % version % Test

    object http {
      private val version: String = "10.2.10"
      val http = group %% "akka-http" % version
      val core = group %% "akka-http-core" % version
      val support = group %% "akka-http2-support" % version
    }
  }

  private object google {
    private val group: String = "com.google.protobuf"
    private val version: String = "3.21.1"
    val protobuf = group % "protobuf-java" % version
  }

  private object io {
    private val group: String = "io.grpc"
    private val version: String = "1.48.1"
    val core = group % "grpc-core" % version
    val netty = group % "grpc-netty-shaded" % version
    val protobuf = group % "grpc-protobuf" % version
  }

  private object logback {
    val chQos = "ch.qos.logback" % "logback-classic" % "1.4.7"
  }

  private object lightbend {
    private val group = "com.lightbend.akka"
    private val version = "1.2.5"

    object management {
      private val managementGroup = s"$group.management"
      private val version = "1.1.4"
      val management = managementGroup %% "akka-management" % version
      val clusterBootstrap = managementGroup %% "akka-management-cluster-bootstrap" % version
    }

    object projection {
      val core = group %% "akka-projection-core" % version
      val eventSourced = group %% "akka-projection-eventsourced" % version
      val kafka = group %% "akka-projection-kafka" % version
      val slick = group %% "akka-projection-slick" % version
      val testKit = "com.lightbend.akka" %% "akka-projection-testkit" % version % Test
    }
  }

  private val akkaDiscoveryKubernetesApi = "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % "1.0.10"

  private val macwire = "com.softwaremill.macwire" % "macros_2.13" % "2.3.7" % "provided"

  private object pureconfig {
    private val group = "com.github.pureconfig"
    private val version = "0.17.4"
    val core = group %% "pureconfig" % version
    val akka = group %% "pureconfig-akka" % version
  }

  private val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4"

  private val scalaPB = "com.thesamet.scalapb" %% "scalapb-runtime" % "0.11.11"

  private val scalaTest = "org.scalatest" %% "scalatest" % "3.2.9" % Test

  private object refined {
    private val group = "eu.timepit"
    private val version = "0.11.0"
    val core = group %% "refined" % version
    val pureconfig = group %% "refined-pureconfig" % version
  }

  private object slick {
    private val group: String = "com.typesafe.slick"
    private val version: String = "3.3.3"
    val core = group %% "slick" % version
    val hikari = group %% "slick-hikaricp" % version
  }

  private object TestContainers {
    private val group = "com.dimafeng"
    private val version = "0.39.5"
    val core = group %% "testcontainers-scala-scalatest" % version % Test
    val postgresql = group %% "testcontainers-scala-postgresql" % version % Test
  }

  private val tminglei = "com.github.tminglei" %% "slick-pg" % "0.19.6"

  //===============================================================================================
  // Service dependencies
  //===============================================================================================

  private val implCore: Seq[ModuleID] = Seq(
    akka.actor,
    akka.actorTyped,
    akka.clusterShardingTyped,
    akka.clusterTyped,
    akka.discovery,
    akka.jackson,
    akka.http.http,
    akka.http.core,
    akka.http.support,
    akka.persistenceCassandra,
    akka.persistenceQuery,
    akka.persistenceTyped,
    akkaDiscoveryKubernetesApi,
    google.protobuf,
    logback.chQos,
    io.core,
    io.netty,
    io.protobuf,
    lightbend.management.management,
    lightbend.management.clusterBootstrap,
    lightbend.projection.core,
    lightbend.projection.eventSourced,
    lightbend.projection.kafka,
    lightbend.projection.slick,
    macwire,
    pureconfig.core,
    pureconfig.akka,
    refined.core,
    refined.pureconfig,
    scalaLogging,
    scalaPB,
    slick.core,
    slick.hikari,
    tminglei
  )

  private val implTest = Seq(
    akka.persistenceTestKit,
    akka.streamTestKit,
    akka.testKitTyped,
    lightbend.projection.testKit,
    scalaTest,
    TestContainers.core,
    TestContainers.postgresql,
  )

  val impl: Seq[sbt.ModuleID] = implCore ++ implTest
}
