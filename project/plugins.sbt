// Add akka-grpc plugin
// NOTE, this pluging version has dependencies that require scalaxml to be v1.x
// Bumping to version 2.3.0 actually updates the xml dependency
addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "2.1.0")

// The scoverage plugin for test coverage
// https://github.com/scoverage/sbt-scoverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.9.3")

// The native packager plugin for building the docker container.
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.1")

addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.1.1")