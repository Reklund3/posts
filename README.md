# Posts

Project for managing forum threads on Your Game of the Year

# Docker
### Local Dev
``` console
docker run -e POSTGRES_DB="post" -p 127.0.0.1:5432:5432/tcp postgres:9.6.12
```

# API End Points

## Posts

/post/info

### Testing Create Endpoint

/posts/create/...

```console
curl -d '{"userId":"5C6AE850-977F-43E2-AAC3-F8AE52EDE4BF","body":"testing the body"}' -H "Content-Type: application/json" -X POST http://localhost:8080/post/create
```

```console
grpcurl -d '{"userId":"5C6AE850-977F-43E2-AAC3-F8AE52EDE4BF","body":"testing the body"}' -v -plaintext -import-path impl/src/main/protobuf -proto service.proto 127.0.0.1:8080 posts.PostService.CreatePost
```

/posts/get/:id

## Threads

/threads/... \
/threads/create/... \
/threads/list