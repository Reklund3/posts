lazy val commonSettings = Seq(
  scalaVersion := "2.13.12",
  organization := "org.ygoty",
  organizationName := "ygoty",
  maintainer := "Robert Eklund <robert.eklund3@gmail.com>",
)

lazy val dockerSettings = Seq(
  //TODO: Get a different image, It needs to have JDK11.
  dockerBaseImage := "eclipse-temurin:11.0.20.1_1-jre-focal",
  dockerUpdateLatest := true,
  dockerExposedPorts ++= Seq(8080, 9001, 8558, 25520),
  dockerUsername := sys.props.get("docker.username"),
  dockerRepository := sys.props.get("docker.registry"),
  //    dockerCommands := Seq(
  //
  //    ),
)

//TODO: get the docker packaging together to start up service once deployed
lazy val baseProjectSettings: Seq[Def.Setting[_]] =
  Seq(
    Universal / javacOptions ++= Seq(
      // print the config to the log on startup
      "-Dconfig.trace=loads",
      // set the service name for the akka cluster bootstrapping
      s"-Dakka.management.cluster.bootstrap.contact-point-discovery.service-name=${name.value}",
      // enable cgroups memory management
      "-J-XX:+UnlockExperimentalVMOptions",
      "-J-XX:+UseCGroupMemoryLimitForHeap"
    )
  )

lazy val posts = (project in file("."))
  .settings(commonSettings: _*)
  .aggregate(`impl`)

lazy val `impl` = (project in file("impl"))
  .enablePlugins(AkkaGrpcPlugin, DockerPlugin, JavaServerAppPackaging)
  .settings(commonSettings: _*)
  .settings(baseProjectSettings: _*)
  .settings(dockerSettings: _*)
  .settings(
    name := "posts",
    Compile / mainClass := Some("org.ygoty.posts.impl.PostsHttpServer"),
    run / mainClass := Some("org.ygoty.posts.impl.PostsHttpServer"),
    dynverSeparator := "-",
    //    run / fork := true,
  )
  .settings(
    libraryDependencies ++= Dependencies.impl
  )
